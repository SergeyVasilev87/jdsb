package projectGroovyScript.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projectGroovyScript.entities.GroovyScriptEntity;

@Repository
public interface ScriptRepository extends JpaRepository<GroovyScriptEntity, Integer> {

}
