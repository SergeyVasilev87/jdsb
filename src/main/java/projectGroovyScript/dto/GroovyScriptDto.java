package projectGroovyScript.dto;

import lombok.Data;

@Data
public class GroovyScriptDto {

    private Integer id;

    private String name;

    private String scriptText;
}
