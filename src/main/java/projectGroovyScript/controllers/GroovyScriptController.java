package projectGroovyScript.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import projectGroovyScript.dto.GroovyScriptDto;
import projectGroovyScript.services.GroovyScriptService;

import java.io.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/groovy")
public class GroovyScriptController {

    private final GroovyScriptService groovyScriptService;

    @PostMapping("/post")
    public ResponseEntity<GroovyScriptDto> addScriptToDB(@RequestParam(value = "name") String name,
                                                         @RequestParam(value = "file") MultipartFile file) throws IOException {
        return ResponseEntity.ok(groovyScriptService.addScriptToDB(name, file));
    }

    @PostMapping("/postJson")
    public ResponseEntity<GroovyScriptDto> addScriptJsonToDB(@RequestBody GroovyScriptDto groovyScriptDto) throws IOException {
        return ResponseEntity.ok(groovyScriptService.addScriptJsonToDB(groovyScriptDto));
    }

    @PutMapping("/put/{id}")
    public Object executeScript(@PathVariable("id") Integer scriptId) {
        return groovyScriptService.executeScript(scriptId);
    }

    @GetMapping("/get")
    public List<GroovyScriptDto> getAllScripts() {
        return groovyScriptService.getAllScripts();
    }
}