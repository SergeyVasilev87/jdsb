package projectGroovyScript.services;

import groovy.lang.GroovyShell;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import projectGroovyScript.dto.GroovyScriptDto;
import projectGroovyScript.entities.GroovyScriptEntity;
import projectGroovyScript.repositories.ScriptRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
public class GroovyScriptService {

    private final ScriptRepository scriptRepository;
    private final GroovyShell groovyShell;



    public GroovyScriptDto addScriptToDB(String name, MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        String str = new String(bytes, StandardCharsets.UTF_8);
        return mapToDto(scriptRepository.save(new GroovyScriptEntity().setName(name).setScriptText(str)));
    }

    public GroovyScriptDto addScriptJsonToDB(GroovyScriptDto groovyScriptDto) throws IOException {
        return mapToDto(scriptRepository.save(mapToEntity(groovyScriptDto)));
    }

    public List<GroovyScriptDto> getAllScripts() {
        return scriptRepository.findAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public Object executeScript(Integer scriptId) {
        try {
            return groovyShell.evaluate(scriptRepository.findById(scriptId).orElse(new GroovyScriptEntity()).getScriptText());
        } catch (Exception e) {
            log.warn("ввод несуществующего скрипта ID {}",scriptId);
            return ResponseEntity.status(404).body("Скрипт не найден. Проверьте вводимый ID");
        }
    }


    private GroovyScriptEntity mapToEntity(GroovyScriptDto groovyScriptDto) {
        GroovyScriptEntity groovyScriptEntity = new GroovyScriptEntity();
        groovyScriptEntity.setId(groovyScriptDto.getId());
        groovyScriptEntity.setName(groovyScriptDto.getName());
        groovyScriptEntity.setScriptText(groovyScriptDto.getScriptText());
        return groovyScriptEntity;
    }
    private GroovyScriptDto mapToDto(GroovyScriptEntity groovyScriptEntity) {
        GroovyScriptDto groovyScriptDto = new GroovyScriptDto();
        groovyScriptDto.setId(groovyScriptEntity.getId());
        groovyScriptDto.setName(groovyScriptEntity.getName());
        groovyScriptDto.setScriptText(groovyScriptEntity.getScriptText());
        return groovyScriptDto;
    }
}
