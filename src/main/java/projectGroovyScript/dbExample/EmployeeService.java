package projectGroovyScript.dbExample;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService implements CommandLineRunner {

    private final EmployeeRepository repository;

    @Override
    public void run(String... args) throws Exception {
        List<EmployeeEntity> list = Arrays.asList(new EmployeeEntity().setName("Employee 1").setRank(2).setSalary(1000),
                new EmployeeEntity().setName("Employee 2").setRank(1).setSalary(800),
                new EmployeeEntity().setName("Employee 3").setRank(2).setSalary(1000),
                new EmployeeEntity().setName("Employee 4").setRank(3).setSalary(1300),
                new EmployeeEntity().setName("Employee 5").setRank(5).setSalary(1900),
                new EmployeeEntity().setName("Employee 6").setRank(1).setSalary(800),
                new EmployeeEntity().setName("Employee 7").setRank(4).setSalary(1500));

        repository.saveAll(list);
    }
}
